using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;

public class JumpScript : MonoBehaviour
{
    public float JumpForce = 0;
    public float TorqueAmount = 1;
    public float ScaleMultiplier = 1.1f;
    private PlayerInput inp;
    private InputAction TorqueAction;
    private Rigidbody2D rb;
    private void Awake()
    {
        if (TryGetComponent<PlayerInput>(out inp))
        {
            inp.onActionTriggered += Inp_onActionTriggered;
            TorqueAction = inp.currentActionMap.FindAction("TorqueAxis", true);
        }
        TryGetComponent<Rigidbody2D>(out rb);
    }

    private void Inp_onActionTriggered(InputAction.CallbackContext obj)
    {
        if (obj.phase == InputActionPhase.Performed)
        {
            if (obj.action.name == "Jump")
            {
                Jump();
            }
            else if(obj.action.name == "ScrollAxis")
            {
                Scroll(obj);
            }
        }
    }

    private void Scroll(InputAction.CallbackContext obj)
    { 
        if(transform != null)
        {
            float factor = ScaleMultiplier;
            Vector3 scale = transform.localScale;
            float axis = obj.ReadValue<float>();
            if (axis > 0)
            {
                factor *= axis;
                scale *= factor;
            }
            else
            {
                factor *= math.abs(-axis);
                scale /= factor;
            }
            transform.localScale = scale;
        }
    }
    private void Jump()
    {
        if (rb != null)
        {
            rb.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
        }
    }
    private void Update()
    {
        if (TorqueAction != null)
        {
            var axisValue = TorqueAction.ReadValue<float>();
            rb.AddTorque(axisValue * TorqueAmount);
        }
    }
}
